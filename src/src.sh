#!/usr/bin/env bash

declare -a HTTP_RESPONSE=(
   [200]="OK"
   [201]="Created"
   [400]="Bad Request"
   [403]="Forbidden"
   [404]="Not Found"
   [405]="Method Not Allowed"
   [417]="Expectation Failed"
   [500]="Internal Server Error"
)

recv () {
  echo "< $@" >&2
}
send () {
  echo "> $@" >&2
  printf '%s\r\n' "$*"
}
fail_with () {
   send_response "$1"
   exit 1
}
send_response_ok_exit () {
  send_response 200
  exit 0
}
serve_hello() {
   add_response_header "Content-Type" "text/plain"
   send_response_ok_exit "Hello, $2!"
}
on_uri_match () {
   local regex=$1
   shift

   [[ $REQUEST_URI =~ $regex ]] && \
      "$@" "${BASH_REMATCH[@]}"
}
unconditionally () {
   "$@" "$REQUEST_URI"
}
send_response () {
   local status_code=$1
   local status_text=${HTTP_RESPONSE[$code]}

   send "HTTP/1.0 $status_code $status_text"
   for i in "${RESPONSE_HEADERS[@]}"; do
      send "$i"
   done
   send
   while read -r line; do
      send "$line"
   done
}
